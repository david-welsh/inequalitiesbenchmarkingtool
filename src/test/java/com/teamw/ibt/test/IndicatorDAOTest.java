package com.teamw.ibt.test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.CategoryDAOImpl;
import com.teamw.ibt.dao.impl.IndicatorDAOImpl;
import com.teamw.ibt.model.Category;
import com.teamw.ibt.model.Indicator;

public class IndicatorDAOTest extends DAOTest {
	
	private IndicatorDAOImpl indicatorDAOImpl;
	private CategoryDAOImpl categoryDAOImpl;
	
	@Before
	public void setUp() {
		super.setUp();
		categoryDAOImpl = new CategoryDAOImpl(jdbcTemplate);
		indicatorDAOImpl = new IndicatorDAOImpl(jdbcTemplate, categoryDAOImpl);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}
	
	@Test
	public void testInsert() {
		String query = "SELECT COUNT(*) AS num FROM IBT.INDICATOR";
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(0, count);
		
		Category category = new Category("Category");
		categoryDAOImpl.insert(category);
		indicatorDAOImpl.insert(new Indicator("Indicator 1", "Source 1", true, 1, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 2", "Source 1", true, 2, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 3", "Source 3", true, 3, category, "restrict"));
		
		count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(3, count);
	}
	
	@Test
	public void getByID() {
		Category category = new Category("Category");
		categoryDAOImpl.insert(category);
		indicatorDAOImpl.insert(new Indicator("Indicator 1", "Source 1", true, 41, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 2", "Source 1", true, 42, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 3", "Source 3", true, 43, category, "restrict"));
		
		Indicator indicator = indicatorDAOImpl.getById(41);
		
		assertEquals((Integer) 41, indicator.getId());
		assertEquals("Indicator 1", indicator.getIndicator());
		assertEquals("Category", indicator.getCategory().getName());
		assertTrue(indicator.getRate());
		assertEquals("Source 1", indicator.getSource());
	}
	
	@Test
	public void getByCatTest() {
		Category category = new Category("Category 1");
		Category category2 = new Category("Category 2");
		categoryDAOImpl.insert(category);
		categoryDAOImpl.insert(category2);
		indicatorDAOImpl.insert(new Indicator("Indicator 1", "Source 1", true, 41, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 2", "Source 1", true, 42, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 3", "Source 3", true, 43, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 4", "Source 3", true, 44, category2, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 5", "Source 3", true, 45, category2, "restrict"));
		
		List<Indicator> indicators = indicatorDAOImpl.getByCategory(category2);
		assertEquals(2, indicators.size());
		for (int i = 0; i < indicators.size(); i++){
			assertEquals((Integer) (44 + i), indicators.get(i).getId());
			assertEquals("Indicator " + (i + 4), indicators.get(i).getIndicator());
			assertTrue(indicators.get(i).getRate());
			assertEquals("Source 3", indicators.get(i).getSource());
		}
	}
	
	@Test
	public void getAllTest() {
		Category category = new Category("Category 1");
		Category category2 = new Category("Category 2");
		categoryDAOImpl.insert(category);
		categoryDAOImpl.insert(category2);
		indicatorDAOImpl.insert(new Indicator("Indicator 1", "Source 1", true, 41, category, "Age"));
		indicatorDAOImpl.insert(new Indicator("Indicator 2", "Source 1", true, 42, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 3", "Source 3", true, 43, category, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 4", "Source 3", true, 44, category2, "restrict"));
		indicatorDAOImpl.insert(new Indicator("Indicator 5", "Source 3", true, 45, category2, "restrict"));
		
		List<Indicator> indicators = indicatorDAOImpl.getAll();
		assertEquals(5, indicators.size());
		for (int i = 0; i < indicators.size(); i++){
			assertEquals((Integer) (41 + i), indicators.get(i).getId());
			assertEquals("Indicator " + (i + 1), indicators.get(i).getIndicator());
			assertTrue(indicators.get(i).getRate());
		}
	}
}
