package com.teamw.ibt.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.RegionDAOImpl;
import com.teamw.ibt.model.Region;

public class RegionDAOTest extends DAOTest {
 
	 private RegionDAOImpl regionDAOImpl;
	 
	 @Before
	 public void setUp(){
		 super.setUp();
		 regionDAOImpl = new RegionDAOImpl(jdbcTemplate);
	 }
	 
	 @After
	 public void tearDown(){
		 super.tearDown();
	 }
	 
	 @Test
	 public void testContext() {
	     assertNotNull(regionDAOImpl);
	 }
	 
	 @Test
	 public void insertTest() {
		 regionDAOImpl.insert(new Region("Region 3", "3"));
		 String query = "SELECT COUNT(*) AS num FROM IBT.REGION";
		 jdbcTemplate.execute(query);
		 int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
				@Override
				public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
					if (rs.next()) {
						return rs.getInt("num");
					}

					return null;
				}
			});
		 
		 assertEquals(1, count);
	 }
	 
	 @Test
	 public void FindByCodeTest() {
		regionDAOImpl.insert(new Region("Region 1", "1"));
		regionDAOImpl.insert(new Region("Region 2", "2"));
		
	 	Region region = regionDAOImpl.findByCode("1");
	 	assertEquals("Region 1", region.getName());
		assertEquals("1", region.getCode());
	 }

}
