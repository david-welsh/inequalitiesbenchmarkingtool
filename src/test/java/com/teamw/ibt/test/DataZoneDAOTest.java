package com.teamw.ibt.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.DataZoneDAOImpl;
import com.teamw.ibt.dao.impl.IntZoneDAOImpl;
import com.teamw.ibt.dao.impl.LocalityDAOImpl;
import com.teamw.ibt.dao.impl.RegionDAOImpl;
import com.teamw.ibt.model.DataZone;
import com.teamw.ibt.model.IntZone;
import com.teamw.ibt.model.Locality;
import com.teamw.ibt.model.Region;
import com.teamw.ibt.test.stubs.LocalityStub;
import com.teamw.ibt.test.stubs.RegionStub;

public class DataZoneDAOTest extends DAOTest {
	
	private IntZoneDAOImpl intZoneDAOImpl;
	private LocalityDAOImpl localityDAOImpl;
	private RegionDAOImpl regionDAOImpl;
	private Region regionStub;
	private Locality localityStub;
	private DataZoneDAOImpl dataZoneDAOImpl;
	
	@Before
	public void setUp() {
		super.setUp();
		regionStub = new RegionStub("Stub", "1");
		regionDAOImpl = new RegionDAOImpl(jdbcTemplate);
		localityDAOImpl = new LocalityDAOImpl(jdbcTemplate, regionDAOImpl);
		intZoneDAOImpl = new IntZoneDAOImpl(jdbcTemplate, localityDAOImpl);
		localityStub = new LocalityStub("Stub Local", "1", regionStub);
		dataZoneDAOImpl = new DataZoneDAOImpl(jdbcTemplate, intZoneDAOImpl);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}

	@Test
	public void testContext() {
		assertNotNull(dataZoneDAOImpl);
	}
	
	@Test
	public void testInsert() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		IntZone zone = new IntZone("Zone 1", "1", localityStub);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("Dzone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		
		
		String query = "SELECT COUNT(*) AS num FROM IBT.DATAZONE";
		
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);		
	}
	
	@Test
	public void testGetAll() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		IntZone zone = new IntZone("Zone 1", "1", localityStub);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("Dzone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		dataZoneDAOImpl.insert(new DataZone("Dzone 2", "2", zone));
		dataZoneDAOImpl.insert(new DataZone("Dzone 3", "3", zone));
		
		List<DataZone> zones = dataZoneDAOImpl.getAll();
		assertEquals(3, zones.size());
		for (int i=0; i<3; i++){
			assertEquals("Dzone " + (i+1), zones.get(i).getName());
			assertEquals("" + (i+1), zones.get(i).getCode());
		}
	}
	
	@Test
	public void testInRegion() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		IntZone zone = new IntZone("Zone 1", "1", localityStub);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("Dzone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		dataZoneDAOImpl.insert(new DataZone("Dzone 2", "2", zone));
		dataZoneDAOImpl.insert(new DataZone("Dzone 3", "3", zone));
		
		List<DataZone> zones = dataZoneDAOImpl.getInRegion(regionStub);
		assertEquals(3, zones.size());
		for (int i=0; i<3; i++){
			assertEquals("Dzone " + (i+1), zones.get(i).getName());
			assertEquals("" + (i+1), zones.get(i).getCode());
		}
	}
	
	@Test
	public void testInLocality() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		IntZone zone = new IntZone("Zone 1", "1", localityStub);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("Dzone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		dataZoneDAOImpl.insert(new DataZone("Dzone 2", "2", zone));
		dataZoneDAOImpl.insert(new DataZone("Dzone 3", "3", zone));
		
		List<DataZone> zones = dataZoneDAOImpl.getInLocality(localityStub);
		assertEquals(3, zones.size());
		for (int i=0; i<3; i++){
			assertEquals("Dzone " + (i+1), zones.get(i).getName());
			assertEquals("" + (i+1), zones.get(i).getCode());
		}
	}
	
	@Test
	public void testInIntZone() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		IntZone zone = new IntZone("Zone 1", "1", localityStub);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("Dzone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		dataZoneDAOImpl.insert(new DataZone("Dzone 2", "2", zone));
		dataZoneDAOImpl.insert(new DataZone("Dzone 3", "3", zone));
		
		List<DataZone> zones = dataZoneDAOImpl.getInIntZone(zone);
		assertEquals(3, zones.size());
		for (int i=0; i<3; i++){
			assertEquals("Dzone " + (i+1), zones.get(i).getName());
			assertEquals("" + (i+1), zones.get(i).getCode());
		}
	}
}
