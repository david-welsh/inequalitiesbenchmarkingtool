package com.teamw.ibt.test;

import static org.junit.Assert.assertArrayEquals;
import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.util.Random;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.AnnotationConfigContextLoader;

import com.teamw.ibt.Benchmarker;
import com.teamw.ibt.test.stubs.BenchmarkerStub;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = Benchmarker.class, loader = AnnotationConfigContextLoader.class)
public class BenchmarkerTest {

	@Autowired
	private Benchmarker benchmarker;
	private BenchmarkerStub benchmarkerStub;

	@Before
	public void setUp() {
		benchmarkerStub = new BenchmarkerStub();
		System.out.println("-----> SETUP <-----");
	}

	@Test
	public void testSampleBenchmarker() {
		assertEquals("class com.teamw.ibt.Benchmarker", this.benchmarker.getClass().toString());
	}

	/** Testing current implementation of the Benchmarking algorithm
	 *  against a given implementation
	 */
	@Test
	public void testBenchmarkingAlgorigthm() {
		int numOfTests = 10;
		int popBound = 1000;
		for (int i = 0; i < numOfTests; i++) {
			int totalPopulation = new Random().nextInt(popBound);
			int deprivedPopulation = new Random().nextInt(popBound);
			int groupSize = new Random().nextInt(popBound);
			assertArrayEquals(benchmarkerStub.benchmark(totalPopulation, deprivedPopulation, groupSize),
					Benchmarker.benchmarkPopulation(totalPopulation, deprivedPopulation, groupSize));
		}
	}
	
	/** Testing with negative totalPopulation
	 *  It should still return an array with a result
	 *  Integrity checks are done in the database functions
	 */ 
	@Test
	public void testBenchmarkingAlgorigthmNegative() {
		int totalPopulation = -100;
		int deprivedPopulation = 20;
		int groupSize = 10;
		assertTrue(Benchmarker.benchmarkPopulation(totalPopulation, deprivedPopulation, groupSize)
				instanceof long[]);
	}
	
	/** Testing with a deprivedPopulation larger than totalPopulation
	 *  It should still return an array with a result
	 *  Integrity checks are done in the database functions
	 */ 
	@Test
	public void testBenchmarkingAlgorigthmLargerDeprived() {
		int totalPopulation = 100;
		int deprivedPopulation = 120;
		int groupSize = 10;
		assertTrue(Benchmarker.benchmarkPopulation(totalPopulation, deprivedPopulation, groupSize)
				instanceof long[]);
	}

	@After
	public void afterTest() {
		benchmarkerStub = null;
		System.out.println("-----> DESTROY <-----");
	}
}
