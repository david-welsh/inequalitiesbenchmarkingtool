package com.teamw.ibt.test;

import static com.jayway.restassured.RestAssured.*;
import static org.hamcrest.Matchers.*;


import org.apache.http.HttpStatus;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.test.IntegrationTest;
import org.springframework.boot.test.SpringApplicationConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.web.WebAppConfiguration;
import org.apache.commons.io.IOUtils;

import com.jayway.restassured.RestAssured;
import com.jayway.restassured.authentication.FormAuthConfig;
import com.teamw.ibt.InequalitiesBenchmarkingTool;

@RunWith(SpringJUnit4ClassRunner.class)
@SpringApplicationConfiguration(classes = InequalitiesBenchmarkingTool.class)
@WebAppConfiguration
@IntegrationTest("server.port:0")
public class ControllerTest {
	
	private static final String HOME = "/";
	private static final String ABOUT = "/about";
	private static final String METHOD = "/method";
	private static final String ADMIN = "/admin";
	
	@Value("${local.server.port}")
	private int serverPort;
	
	@Before
	public void setUp() {
	  RestAssured.port = serverPort;
	}
	
	@Test
	public void homeTest() {
		when()
			.get(HOME)
		.then()
	    	.statusCode(HttpStatus.SC_OK)
	    	.contentType("text/html")
	    	.body(containsString("Inequalities Benchmarking Tool"));
	}
	
	@Test
	public void aboutTest() {
		when()
	    	.get(ABOUT)
	    .then()
	    	.statusCode(HttpStatus.SC_OK)
	    	.body(containsString("About"));;
	}
	
	@Test
	public void methodTest() {
		when()
	    	.get(METHOD)
	    .then()
	    	.statusCode(HttpStatus.SC_OK)
	    	.body(containsString("Method"));
	}
	
	@Test
	public void redirectTest() {
		when()
	    	.get(ADMIN)
	    .then()
	    	.statusCode(HttpStatus.SC_OK)
	    	.body(containsString("Login"));
	}
	
//	@Test
//    public void formAuthenticationWithDefinedCsrfField() throws Exception {
//        given().
//                auth().form("admin", "password", new FormAuthConfig("j_spring_security_check_with_csrf", "username", "password")
//                		.withAutoDetectionOfCsrf()).
//        when().
//                get("/login").
//        then().
//                statusCode(200).
//                body(containsString("Tool"));
//    }
	
	 @Test
	 public void doesntFollowRedirectsIfExplicitlySpecified() throws Exception {
	     given().
	             redirects().follow(false).
	             param("url", "/login").
	     expect().
	             statusCode(302).
	             header("Location", is("http://localhost:" + serverPort + "/login")).
	     when().
	             get(ADMIN);
	 }
	
	@Test
	public void testAuthentication() {
		expect().
			statusCode(200).
			body(containsString("Login")).
		when().
			get(ADMIN);
	}
}
