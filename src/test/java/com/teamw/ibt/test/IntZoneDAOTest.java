package com.teamw.ibt.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.LocalityDAOImpl;
import com.teamw.ibt.dao.impl.RegionDAOImpl;
import com.teamw.ibt.dao.impl.IntZoneDAOImpl;
import com.teamw.ibt.model.IntZone;
import com.teamw.ibt.model.Locality;
import com.teamw.ibt.model.Region;
import com.teamw.ibt.test.stubs.LocalityStub;
import com.teamw.ibt.test.stubs.RegionStub;

public class IntZoneDAOTest extends DAOTest {
	
	private IntZoneDAOImpl intZoneDAOImpl;
	private LocalityDAOImpl localityDAOImpl;
	private RegionDAOImpl regionDAOImpl;
	private Region regionStub;
	private Locality localityStub;
	
	@Before
	public void setUp() {
		super.setUp();
		regionStub = new RegionStub("Stub", "1");
		regionDAOImpl = new RegionDAOImpl(jdbcTemplate);
		localityDAOImpl = new LocalityDAOImpl(jdbcTemplate, regionDAOImpl);
		intZoneDAOImpl = new IntZoneDAOImpl(jdbcTemplate, localityDAOImpl);
		localityStub = new LocalityStub("Stub Local", "1", regionStub);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}

	@Test
	public void testContext() {
		assertNotNull(localityDAOImpl);
	}
	
	@Test
	public void testInsert() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		intZoneDAOImpl.insert(new IntZone("Zone 1", "1", localityStub));
		String query = "SELECT COUNT(*) AS num FROM IBT.LOCALITY";
		jdbcTemplate.execute(query);
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);		
	}
	
	@Test
	public void testFindByCode() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		intZoneDAOImpl.insert(new IntZone("Zone 1", "1", localityStub));
		
		Region region = new Region("Region 2", "2");
		regionDAOImpl.insert(region);
		Locality locality = new Locality("Locality 2", "2", region);
		localityDAOImpl.insert(locality);
		intZoneDAOImpl.insert(new IntZone("Zone 2", "2", locality));
		
		IntZone intZone = intZoneDAOImpl.findByCode("2");
		assertEquals("Zone 2", intZone.getName());
		assertEquals("2", intZone.getCode());
		assertEquals("Locality 2", intZone.getLocality().getName());
		assertEquals("2", intZone.getLocality().getCode());
	}
	
	@Test
	public void findByLocalityTest() {
		regionDAOImpl.insert(regionStub);
		localityDAOImpl.insert(localityStub);
		intZoneDAOImpl.insert(new IntZone("Zone 1", "1", localityStub));
		
		
		Region region = new Region("Region 2", "2");
		regionDAOImpl.insert(region);
		Locality locality = new Locality("Locality 2", "2", region);
		localityDAOImpl.insert(locality);
		intZoneDAOImpl.insert(new IntZone("Zone 2", "2", locality));
		
		intZoneDAOImpl.insert(new IntZone("Zone 3", "3", localityStub));
		
		List<IntZone> intZone = intZoneDAOImpl.findByLocality(localityStub);
		
		for (IntZone zone : intZone) {
			assertEquals("1", zone.getLocality().getCode());
			assertEquals("Stub Local", zone.getLocality().getName());
			assertEquals("Stub", zone.getLocality().getRegion().getName());
			assertEquals("1", zone.getLocality().getRegion().getCode());
		}
	}

}
