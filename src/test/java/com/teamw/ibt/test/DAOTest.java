package com.teamw.ibt.test;

import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabase;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseBuilder;
import org.springframework.jdbc.datasource.embedded.EmbeddedDatabaseType;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:app-context-test.xml")
public class DAOTest {
	
	public EmbeddedDatabase db;
	public JdbcTemplate jdbcTemplate;
	
	@Before
	 public void setUp(){
		 db = new EmbeddedDatabaseBuilder()
		    		.setType(EmbeddedDatabaseType.H2)
		    		.addScript("schema.sql")
		    		.build();
		 jdbcTemplate = new JdbcTemplate(db);
	 }
	
	@After
	public void tearDown(){
		db.shutdown();
	}

	@Test
	public void jdbcNotNull() {
		assertNotNull(jdbcTemplate);
	}

}
