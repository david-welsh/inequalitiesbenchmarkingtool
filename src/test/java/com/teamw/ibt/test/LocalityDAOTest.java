package com.teamw.ibt.test;

import static org.junit.Assert.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.LocalityDAOImpl;
import com.teamw.ibt.dao.impl.RegionDAOImpl;
import com.teamw.ibt.model.Locality;
import com.teamw.ibt.model.Region;
import com.teamw.ibt.test.stubs.RegionStub;

public class LocalityDAOTest extends DAOTest {
	
	private LocalityDAOImpl localityDAOImpl;
	private Region stub;
	private RegionDAOImpl regionDAOImpl;
	
	@Before
	public void setUp() {
		super.setUp();
		stub = new RegionStub("Stub", "1");
		regionDAOImpl = new RegionDAOImpl(jdbcTemplate);
		localityDAOImpl = new LocalityDAOImpl(jdbcTemplate, regionDAOImpl);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}

	@Test
	public void testContext() {
		assertNotNull(localityDAOImpl);
	}
	
	@Test
	public void insertTest() {
		regionDAOImpl.insert(stub);
		localityDAOImpl.insert(new Locality("Locality 1", "2", stub));
		String query = "SELECT COUNT(*) AS num FROM IBT.LOCALITY";
		jdbcTemplate.execute(query);
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);		
	}

	@Test
	public void findByCodeTest() {
		regionDAOImpl.insert(stub);
		localityDAOImpl.insert(new Locality("Locality 1", "2", stub));
		
		Region region = new Region("Region 1", "2");
		regionDAOImpl.insert(region);
		localityDAOImpl.insert(new Locality("Locality 2", "3", region));
		
		Locality locality = localityDAOImpl.findByCode("2");
		assertEquals("Locality 1", locality.getName());
		assertEquals("2", locality.getCode());
		assertEquals("Stub", locality.getRegion().getName());
		assertEquals("1", locality.getRegion().getCode());
	}

	@Test
	public void findByRegionTest() {
		regionDAOImpl.insert(stub);
		localityDAOImpl.insert(new Locality("Locality 1", "2", stub));
		localityDAOImpl.insert(new Locality("Locality 4", "4", stub));
		
		Region region = new Region("Region 1", "2");
		regionDAOImpl.insert(region);
		localityDAOImpl.insert(new Locality("Locality 2", "3", region));
		
		List<Locality> locality = localityDAOImpl.findByRegion(stub);
		
		for (Locality local : locality) {
			assertEquals(stub.getCode(), local.getRegion().getCode());
			assertEquals(stub.getName(), local.getRegion().getName());
		}
	}
	
	

}
