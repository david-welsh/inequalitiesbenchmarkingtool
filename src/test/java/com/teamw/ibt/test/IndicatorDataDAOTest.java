package com.teamw.ibt.test;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.CategoryDAOImpl;
import com.teamw.ibt.dao.impl.DataZoneDAOImpl;
import com.teamw.ibt.dao.impl.IndicatorDAOImpl;
import com.teamw.ibt.dao.impl.IndicatorDataDAOImpl;
import com.teamw.ibt.dao.impl.IntZoneDAOImpl;
import com.teamw.ibt.dao.impl.LocalityDAOImpl;
import com.teamw.ibt.dao.impl.RegionDAOImpl;
import com.teamw.ibt.model.Category;
import com.teamw.ibt.model.DataZone;
import com.teamw.ibt.model.Indicator;
import com.teamw.ibt.model.IndicatorData;
import com.teamw.ibt.model.IntZone;
import com.teamw.ibt.model.Locality;
import com.teamw.ibt.model.Region;

public class IndicatorDataDAOTest extends DAOTest {
	
	private IndicatorDAOImpl indicatorDAOImpl;
	private CategoryDAOImpl categoryDAOImpl;
	private RegionDAOImpl regionDAOImpl;
	private LocalityDAOImpl localityDAOImpl;
	private IntZoneDAOImpl intZoneDAOImpl;
	private DataZoneDAOImpl dataZoneDAOImpl;
	private IndicatorDataDAOImpl indicatorDataDAOImpl;
	
	@Before
	public void setUp() {
		super.setUp();
		categoryDAOImpl = new CategoryDAOImpl(jdbcTemplate);
		indicatorDAOImpl = new IndicatorDAOImpl(jdbcTemplate, categoryDAOImpl);
		regionDAOImpl = new RegionDAOImpl(jdbcTemplate);
		localityDAOImpl = new LocalityDAOImpl(jdbcTemplate, regionDAOImpl);
		intZoneDAOImpl = new IntZoneDAOImpl(jdbcTemplate, localityDAOImpl);
		dataZoneDAOImpl = new DataZoneDAOImpl(jdbcTemplate, intZoneDAOImpl);
		indicatorDataDAOImpl = new IndicatorDataDAOImpl(jdbcTemplate, indicatorDAOImpl, dataZoneDAOImpl);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}
	
	@Test
	public void insertNumTest() {
		String query = "SELECT COUNT(*) AS num FROM IBT.INDICATORDATA";
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(0, count);	
		
		Category category = new Category("Category");
		categoryDAOImpl.insert(category);
		Indicator indicator = new Indicator("Indicator 1", "Source 1", true, 41, category, "Restrict");
		Region region = new Region("Region 1", "1");
		Locality locality = new Locality("Local 1", "1", region);
		indicatorDAOImpl.insert(indicator);
		regionDAOImpl.insert(region);
		localityDAOImpl.insert(locality);
		IntZone zone = new IntZone("Zone 1", "1", locality);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("DZone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		IndicatorData indicatorData = new IndicatorData(indicator, dzone, 1.0, 100);
		
		indicatorDataDAOImpl.insertNum(indicatorData);
		query = "SELECT COUNT(*) AS num FROM IBT.INDICATORDATA";
		count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);	
	}
	
	@Test
	public void insertRateTest() {
		String query = "SELECT COUNT(*) AS num FROM IBT.INDICATORDATA";
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(0, count);	
		
		Category category = new Category("Category");
		categoryDAOImpl.insert(category);
		Indicator indicator = new Indicator("Indicator 1", "Source 1", true, 41, category, "Age");
		Region region = new Region("Region 1", "1");
		Locality locality = new Locality("Local 1", "1", region);
		indicatorDAOImpl.insert(indicator);
		regionDAOImpl.insert(region);
		localityDAOImpl.insert(locality);
		IntZone zone = new IntZone("Zone 1", "1", locality);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("DZone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		IndicatorData indicatorData = new IndicatorData(indicator, dzone, 1.0, 100);
		indicatorDataDAOImpl.insertRate(indicatorData);
		
		query = "SELECT COUNT(*) AS num FROM IBT.INDICATORDATA";
		count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);	
	}
	
	@Test
	public void getFromDataZoneTest() {
		Category category = new Category("Category");
		categoryDAOImpl.insert(category);
		Indicator indicator = new Indicator("Indicator 1", "Source 1", true, 41, category, "Age");
		Region region = new Region("Region 1", "1");
		Locality locality = new Locality("Local 1", "1", region);
		indicatorDAOImpl.insert(indicator);
		regionDAOImpl.insert(region);
		localityDAOImpl.insert(locality);
		IntZone zone = new IntZone("Zone 1", "1", locality);
		intZoneDAOImpl.insert(zone);
		DataZone dzone = new DataZone("DZone 1", "1", zone);
		dataZoneDAOImpl.insert(dzone);
		IndicatorData indicatorData = new IndicatorData(indicator, dzone, 1.0, 100);
		
		indicatorDataDAOImpl.insertNum(indicatorData);
		
		IndicatorData result = indicatorDataDAOImpl.getFromDataZone(dzone, indicator);
		
		assertEquals("DZone 1", result.getDataZone().getName());
		
		
		
	}
	
}
