package com.teamw.ibt.test.stubs;

import com.teamw.ibt.model.Region;

public class RegionStub extends Region {
	

	public RegionStub(String name, String code) {
		super(name, code);
	}

	public String getName(){
		return "Stub";
	}
	
	public String getCode(){
		return "1";
	}
}
