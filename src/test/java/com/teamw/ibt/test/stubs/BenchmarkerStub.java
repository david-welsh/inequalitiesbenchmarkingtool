package com.teamw.ibt.test.stubs;

public final class BenchmarkerStub {
	
	private static final double Z = 1.96;

	public BenchmarkerStub() {
	}

	public long[] benchmark(int totalPopulation, int deprivedPopulation, int groupSize){
		double rate = (float)deprivedPopulation / totalPopulation;
		double expected = rate * groupSize;
		
		double p = expected / groupSize;
		double q = 1 - p;

		double z2 = Math.pow(Z, 2);
		
	 	double x = 2 * expected + z2;
		double y = Z * Math.sqrt(z2 + 4 * expected * q);
		
		double denom = 2 * (groupSize + z2);
		
		long rangeLower = Math.round(((x - y) / denom) * groupSize);
		long rangeUpper = Math.round(((x + y) / denom) * groupSize);
		long expectedLong = Math.round(expected);

		long[] ret = {expectedLong, rangeLower, rangeUpper};

		return ret;
	}
}