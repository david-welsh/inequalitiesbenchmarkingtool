package com.teamw.ibt.test.stubs;

import com.teamw.ibt.model.Locality;
import com.teamw.ibt.model.Region;

public class LocalityStub extends Locality{

	public LocalityStub(String name, String code, Region region) {
		super(name, code, region);
	}
	
	public String getName(){
		return "Stub Local";
	}
	
	public String getCode(){
		return "1";
	}
	
	public Region getRegion() {
		return new RegionStub("Stub", "1");
	}

}
