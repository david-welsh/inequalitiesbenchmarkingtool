package com.teamw.ibt.test;

import static org.junit.Assert.assertEquals;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.ResultSetExtractor;

import com.teamw.ibt.dao.impl.CategoryDAOImpl;
import com.teamw.ibt.model.Category;

public class CategoryDAOTest extends DAOTest {
	
	private CategoryDAOImpl categoryDAOImpl;
	
	@Before
	public void setUp() {
		super.setUp();
		categoryDAOImpl = new CategoryDAOImpl(jdbcTemplate);
	}
	
	@After
	public void tearDown() {
		super.tearDown();
	}

	@Test
	public void insertTest(){
		String query = "SELECT COUNT(*) AS num FROM IBT.CATEGORY";
		int count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(0, count);
		
		categoryDAOImpl.insert(new Category("Category"));
		count = jdbcTemplate.query(query, new ResultSetExtractor<Integer>() {
			@Override
			public Integer extractData(ResultSet rs) throws SQLException, DataAccessException {
				if (rs.next()) {
					return rs.getInt("num");
				}
					return null;
			}
		});
		assertEquals(1, count);
	}
	
	@Test
	public void getByName() {
		categoryDAOImpl.insert(new Category("Category 1"));
		categoryDAOImpl.insert(new Category("Category 2"));
		categoryDAOImpl.insert(new Category("Category 3"));
		
		Category category = categoryDAOImpl.getByName("Category 1");
		assertEquals("Category 1", category.getName());
	}
	
	@Test
	public void getAll() {
		categoryDAOImpl.insert(new Category("Category 1"));
		categoryDAOImpl.insert(new Category("Category 2"));
		categoryDAOImpl.insert(new Category("Category 3"));
		
		List<Category> categories = categoryDAOImpl.getAll();
		assertEquals(3, categories.size());
		
		for (int i = 0; i < categories.size(); i++){
			assertEquals("Category " + (i+1), categories.get(i).getName());
		}
		
	}
}
