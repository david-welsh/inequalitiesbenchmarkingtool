package com.teamw.ibt;
import java.lang.Math;

// A class to calculate the expected deprived population
public final class Benchmarker {

    //1.96 for 95% confidence intervals
	private static final double Z = 1.96;

	//private constructor for final class
	private Benchmarker() {
	}

    /*
     * Method using the formula found on: wiki:Algorithm
     */
	public static long[] benchmarkRate(double rate, int groupSize) {
		double expected = rate * groupSize;

		double p = expected / groupSize;
		double q = 1 - p;

		double z2 = Math.pow(Z, 2);
		
	 	double x = 2 * expected + z2;
		double y = Z * Math.sqrt(z2 + 4 * expected * q);
		
		double denom = 2 * (groupSize + z2);
		
		long rangeLower = Math.round(((x - y) / denom) * groupSize);
		long rangeUpper = Math.round(((x + y) / denom) * groupSize);
		long expectedLong = Math.round(expected);

		long[] ret = {expectedLong, rangeLower, rangeUpper};

		return ret;	
	}

    /*
     * Method using the formula found on: wiki:Algorithm
     * It takes the total population and deprived population to calculate the rate
     */
	public static long[] benchmarkPopulation(int totalPopulation, int deprivedPopulation, int groupSize){
		double rate = (float)deprivedPopulation / totalPopulation;
		return benchmarkRate(rate, groupSize);
	}
}
