package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface CategoryDAO {
	public void insert(Category category);
	public List<Category> getAll();
	public Category getByName(String name);
}
