package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

public interface PopulationDataDAO {
	public PopulationData findByDZ(DataZone dataZone);
}
