package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface LocalityDAO {
	public void insert(Locality locality);
	public Locality findByCode(String code);
	public List<Locality> findByRegion(Region region);
}
