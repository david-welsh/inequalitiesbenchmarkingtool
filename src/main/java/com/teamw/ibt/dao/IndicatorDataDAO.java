package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface IndicatorDataDAO {
	public void insertNum(IndicatorData indicatorData);
	public void insertRate(IndicatorData indicatorData);
	public void mergeNum(IndicatorData indicatorData);
	public void mergeRate(IndicatorData indicatorData);
	public IndicatorData getFromDataZone(DataZone dataZone, Indicator indicator);
}
