package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;
import java.util.List;

public interface RegionDAO {
	public void insert(Region region);
	public Region findByCode(String code);
	public List<Region> getAll();
}
