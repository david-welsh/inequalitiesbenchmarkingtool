package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface DataZoneDAO {
	public void insert(DataZone dataZone);
	public DataZone findByCode(String code);
	public List<DataZone> findByIntZone(IntZone intZone);
	public List<DataZone> getAll();

	public List<DataZone> getInRegion(Region region);
	public List<DataZone> getInLocality(Locality locality);
	public List<DataZone> getInIntZone(IntZone intZone);
}
