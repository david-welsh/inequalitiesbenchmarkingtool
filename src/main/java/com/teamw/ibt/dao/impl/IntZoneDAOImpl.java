package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class IntZoneDAOImpl implements IntZoneDAO {

	private JdbcTemplate jdbcTemplate;

	private final LocalityDAOImpl localityDAO;

	@Autowired
	public IntZoneDAOImpl(JdbcTemplate jdbcTemplate, LocalityDAOImpl localityDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.localityDAO = localityDAO;
	}

	private class IntZoneMapper implements RowMapper<IntZone> {
		public IntZone mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new IntZone(
				rs.getString("name"),
				rs.getString("code"),
				localityDAO.findByCode(rs.getString("inlocality"))
			);
		}
	}	

	public void insert(IntZone intZone) {
		String query = "INSERT INTO IBT.INTZONE (name, code, inlocality) VALUES (?,?,?);"; 
		jdbcTemplate.update(query, intZone.getName(), intZone.getCode(), intZone.getLocality().getCode());
	}
	public IntZone findByCode(String code) {
		String query = "SELECT * FROM IBT.INTZONE WHERE CODE = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{code}, new IntZoneMapper());
	}
	public List<IntZone> findByLocality(Locality locality) {
		String query = "SELECT * FROM IBT.INTZONE WHERE INLOCALITY = ? ORDER BY NAME ASC;";
		List<IntZone> intZoneList = jdbcTemplate.query(query, new Object[]{locality.getCode()}, new IntZoneMapper());
		return intZoneList;
	}
}
