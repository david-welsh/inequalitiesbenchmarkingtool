package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class DataZoneDAOImpl implements DataZoneDAO {

	@Autowired
	protected JdbcTemplate jdbcTemplate;

	@Autowired
	private final IntZoneDAOImpl intZoneDAO;

	@Autowired
	public DataZoneDAOImpl(JdbcTemplate jdbcTemplate, IntZoneDAOImpl intZoneDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.intZoneDAO = intZoneDAO;
	}

	private class DataZoneMapper implements RowMapper<DataZone> {
		public DataZone mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new DataZone(
				rs.getString("name"),
				rs.getString("code"),
				intZoneDAO.findByCode(rs.getString("inintzone"))
			);
		}
	}

	public void insert(DataZone dataZone) {
		String query = "INSERT INTO IBT.DATAZONE (name, code, inintzone) VALUES (?,?,?);"; 
		jdbcTemplate.update(query, dataZone.getName(), dataZone.getCode(), dataZone.getIntZone().getCode());
	}

	public DataZone findByCode(String code) {
		String query = "SELECT * FROM IBT.DATAZONE WHERE CODE = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{code}, new DataZoneMapper());
	}

	public List<DataZone> findByIntZone(IntZone intZone) {
		String query = "SELECT * FROM IBT.DATAZONE WHERE ININTZONE = ? ORDER BY NAME ASC;";
		List<DataZone> dataZoneList = jdbcTemplate.query(query, new Object[]{intZone.getCode()}, new DataZoneMapper());
		return dataZoneList;
	}

	public List<DataZone> getAll() {
		String query = "SELECT * FROM IBT.DATAZONE ORDER BY NAME ASC;";
		List<DataZone> dataZoneList = jdbcTemplate.query(query, new DataZoneMapper());
		return dataZoneList;
	}
	
	public List<DataZone> getInRegion(Region region) {
		String query = "SELECT * FROM IBT.DATAZONE AS d WHERE d.ININTZONE IN (SELECT CODE FROM IBT.INTZONE AS i WHERE i.INLOCALITY IN (SELECT CODE FROM IBT.LOCALITY AS l WHERE l.INREGION IN(SELECT CODE FROM IBT.REGION AS r WHERE r.code = ?)));";
		return jdbcTemplate.query(query, new Object[]{region.getCode()}, new DataZoneMapper());	
	}

	public List<DataZone> getInLocality(Locality locality) {
		String query = "SELECT * FROM IBT.DATAZONE AS d WHERE d.ININTZONE IN (SELECT CODE FROM IBT.INTZONE AS i WHERE i.INLOCALITY IN (SELECT CODE FROM IBT.LOCALITY AS l WHERE l.code = ?));";
		return jdbcTemplate.query(query, new Object[]{locality.getCode()}, new DataZoneMapper());	
	}

	public List<DataZone> getInIntZone(IntZone intZone) {
		String query = "SELECT * FROM IBT.DATAZONE AS d WHERE d.ININTZONE IN (SELECT CODE FROM IBT.INTZONE AS i WHERE i.code = ?);";
		return jdbcTemplate.query(query, new Object[]{intZone.getCode()}, new DataZoneMapper());	
	}
}
