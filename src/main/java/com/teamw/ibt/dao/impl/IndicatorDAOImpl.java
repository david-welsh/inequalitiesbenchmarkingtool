package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.stereotype.Repository;

@Repository
public class IndicatorDAOImpl implements IndicatorDAO{


	@Autowired
	protected JdbcTemplate jdbcTemplate;

	private final CategoryDAOImpl categoryDAO;

	@Autowired
	public IndicatorDAOImpl(JdbcTemplate jdbcTemplate, CategoryDAOImpl categoryDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.categoryDAO = categoryDAO;
	}

	private class IndicatorMapper implements RowMapper<Indicator> {
		public Indicator mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Indicator(
				rs.getString("INDICATORNAME"),
				rs.getString("SOURCE"),
				rs.getBoolean("RATE"),
				rs.getInt("INDID"),
				categoryDAO.getByName(rs.getString("CATEGORY")),
				rs.getString("RESTRICT")
			);
		}
	}

	public void insert(Indicator indicator) {
		String query = "INSERT INTO ibt.indicator (indicatorname, source, rate, category, restrict) VALUES (?,?,?,?,?);";
		jdbcTemplate.update(query, indicator.getIndicator(), indicator.getSource(), indicator.getRate(), indicator.getCategory().getName(), indicator.getRestrict());
	}

	public List<Indicator> getAll() {
		String query = "SELECT * FROM ibt.indicator;";
		List<Indicator> indicatorList = jdbcTemplate.query(query, new IndicatorMapper());
		return indicatorList;
	}

	public Indicator getById(int id) {
		String query = "SELECT * FROM ibt.indicator WHERE indid = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{id}, new IndicatorMapper());
	}

	public List<Indicator> getByCategory(Category category) {
		String query = "SELECT * FROM ibt.indicator WHERE category = ?;";
		List<Indicator> indicatorList = jdbcTemplate.query(query, new Object[]{category.getName()}, new IndicatorMapper());
		return indicatorList;
	}
}
