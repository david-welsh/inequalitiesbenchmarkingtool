package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;

@Repository
public class RegionDAOImpl implements RegionDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public RegionDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private class RegionMapper implements RowMapper<Region> {
		public Region mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Region(
				rs.getString("name"),
				rs.getString("code")
			);
		}
	}

	public void insert(Region region) {
		String query = "INSERT INTO IBT.REGION (code, name) VALUES (?,?);";
		jdbcTemplate.update(query, region.getCode(), region.getName());
	}

	public Region findByCode(String code) {
		String query = "SELECT * FROM IBT.REGION WHERE CODE = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{code}, new RegionMapper());
	}

	public List<Region> getAll() {
		String query = "SELECT * FROM IBT.REGION ORDER BY NAME ASC;";
		return jdbcTemplate.query(query, new RegionMapper());
	}
}
