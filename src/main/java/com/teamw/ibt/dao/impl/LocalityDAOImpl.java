package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class LocalityDAOImpl implements LocalityDAO {

	private JdbcTemplate jdbcTemplate;

	private final RegionDAOImpl regionDAO;

	@Autowired
	public LocalityDAOImpl(JdbcTemplate jdbcTemplate, RegionDAOImpl regionDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.regionDAO = regionDAO;
	}

	private class LocalityMapper implements RowMapper<Locality> {
		public Locality mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Locality(
				rs.getString("name"),
				rs.getString("code"),
				regionDAO.findByCode(rs.getString("inregion"))
			);
		}
	}

	public void insert(Locality locality) {
		String query = "INSERT INTO IBT.LOCALITY (name, code, inregion) VALUES (?,?,?);"; 
		jdbcTemplate.update(query, locality.getName(), locality.getCode(), locality.getRegion().getCode());
	}
	public Locality findByCode(String code) {
		String query = "SELECT * FROM IBT.LOCALITY WHERE CODE = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{code}, new LocalityMapper());
	}
	public List<Locality> findByRegion(Region region) {
		String query = "SELECT * FROM IBT.LOCALITY WHERE INREGION = ? ORDER BY NAME ASC;";
		List<Locality> localityList = jdbcTemplate.query(query, new Object[]{region.getCode()}, new LocalityMapper());
		return localityList;
	}
}
