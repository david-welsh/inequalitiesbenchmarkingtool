package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.List;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

@Repository
public class CategoryDAOImpl implements CategoryDAO {

	@Autowired
	private JdbcTemplate jdbcTemplate;

	@Autowired
	public CategoryDAOImpl(JdbcTemplate jdbcTemplate) {
		this.jdbcTemplate = jdbcTemplate;
	}

	private class CategoryMapper implements RowMapper<Category> {
		public Category mapRow(ResultSet rs, int rowNum) throws SQLException {
			return new Category(
				rs.getString("NAME")
			);
		}
	}

	public void insert(Category category) {
		String query = "INSERT INTO IBT.CATEGORY (name) VALUES (?);";
		jdbcTemplate.update(query, category.getName());
	}

	public List<Category> getAll() {
		String query = "SELECT * FROM IBT.CATEGORY ORDER BY NAME;";
		return jdbcTemplate.query(query, new CategoryMapper()); 
	}

	public Category getByName(String name) {
		String query = "SELECT * FROM IBT.CATEGORY WHERE NAME = ?";
		return jdbcTemplate.queryForObject(query, new Object[]{name}, new CategoryMapper());
	}
}
