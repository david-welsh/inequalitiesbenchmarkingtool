package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;

@Repository
public class PopulationDataDAOImpl implements PopulationDataDAO {

	private JdbcTemplate jdbcTemplate;

	private final DataZoneDAO dataZoneDAO;

	@Autowired
	public PopulationDataDAOImpl(JdbcTemplate jdbcTemplate, DataZoneDAOImpl dataZoneDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.dataZoneDAO = dataZoneDAO;
	}

	private class PopulationDataMapper implements RowMapper<PopulationData> {
		public PopulationData mapRow(ResultSet rs, int rowNum) throws SQLException {
			ArrayList<Integer> male = new ArrayList<Integer>();
			ArrayList<Integer> female = new ArrayList<Integer>();
	
			male.add(rs.getInt("pop0to4m"));
			male.add(rs.getInt("pop5to9m"));
			male.add(rs.getInt("pop10to14m"));
			male.add(rs.getInt("pop15to19m"));
			male.add(rs.getInt("pop20to24m"));
			male.add(rs.getInt("pop25to29m"));
			male.add(rs.getInt("pop30to34m"));
			male.add(rs.getInt("pop35to39m"));
			male.add(rs.getInt("pop40to44m"));
			male.add(rs.getInt("pop45to49m"));
			male.add(rs.getInt("pop50to54m"));
			male.add(rs.getInt("pop55to59m"));
			male.add(rs.getInt("pop60to64m"));
			male.add(rs.getInt("pop65to69m"));
			male.add(rs.getInt("pop70to74m"));
			male.add(rs.getInt("pop75to79m"));
			male.add(rs.getInt("pop80to84m"));
			male.add(rs.getInt("pop85to89m"));
			male.add(rs.getInt("pop90plusm"));
			
			female.add(rs.getInt("pop0to4f"));
			female.add(rs.getInt("pop5to9f"));
			female.add(rs.getInt("pop10to14f"));
			female.add(rs.getInt("pop15to19f"));
			female.add(rs.getInt("pop20to24f"));
			female.add(rs.getInt("pop25to29f"));
			female.add(rs.getInt("pop30to34f"));
			female.add(rs.getInt("pop35to39f"));
			female.add(rs.getInt("pop40to44f"));
			female.add(rs.getInt("pop45to49f"));
			female.add(rs.getInt("pop50to54f"));
			female.add(rs.getInt("pop55to59f"));
			female.add(rs.getInt("pop60to64f"));
			female.add(rs.getInt("pop65to69f"));
			female.add(rs.getInt("pop70to74f"));
			female.add(rs.getInt("pop75to79f"));
			female.add(rs.getInt("pop80to84f"));
			female.add(rs.getInt("pop85to89f"));
			female.add(rs.getInt("pop90plusf"));
			

			return new PopulationData(
				dataZoneDAO.findByCode(rs.getString("fordz")),
				rs.getInt("poptotal"),
				male,
				female
			);
		}
	}

	public PopulationData findByDZ(DataZone dataZone) {
		String query = "SELECT * FROM IBT.POPULATIONDATA WHERE FORDZ = ?;";
		return jdbcTemplate.queryForObject(query, new Object[]{dataZone.getCode()}, new PopulationDataMapper());
	}

}
