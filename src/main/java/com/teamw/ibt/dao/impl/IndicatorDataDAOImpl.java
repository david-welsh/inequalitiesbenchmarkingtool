package com.teamw.ibt.dao.impl;

import com.teamw.ibt.dao.*;
import com.teamw.ibt.model.*;

import java.sql.ResultSet;
import java.sql.SQLException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.ResultSetExtractor;
import org.springframework.stereotype.Repository;
import org.springframework.jdbc.core.RowMapper;

@Repository
public class IndicatorDataDAOImpl implements IndicatorDataDAO{

	@Autowired
	protected JdbcTemplate jdbcTemplate;

	@Autowired
	private final IndicatorDAOImpl indicatorDAO;
	private final DataZoneDAOImpl dataZoneDAO;

	@Autowired
	public IndicatorDataDAOImpl(JdbcTemplate jdbcTemplate, IndicatorDAOImpl indicatorDAO, DataZoneDAOImpl dataZoneDAO) {
		this.jdbcTemplate = jdbcTemplate;
		this.indicatorDAO = indicatorDAO;
		this.dataZoneDAO = dataZoneDAO;
	}

	private class IndicatorDataMapper implements RowMapper<IndicatorData> {
		public IndicatorData mapRow(ResultSet rs, int rowNum) throws SQLException{
			return new IndicatorData(
				indicatorDAO.getById(rs.getInt("INDID")),
				dataZoneDAO.findByCode(rs.getString("FORDZ")),
				rs.getDouble("RATE"),
				rs.getInt("POP")
			);
		}
	}

	public void insertNum(IndicatorData indicatorData) {
		String query = "INSERT INTO ibt.indicatordata (indid, fordz, pop) VALUES (?,?,?);";
		jdbcTemplate.update(query, indicatorData.getIndicator().getId(), indicatorData.getDataZone().getCode(), indicatorData.getPop());
	}

	public void insertRate(IndicatorData indicatorData) {
		String query = "INSERT INTO ibt.indicatordata (indid, fordz, rate) VALUES (?,?,?);";
		jdbcTemplate.update(query, indicatorData.getIndicator().getId(), indicatorData.getDataZone().getCode(), indicatorData.getRate());
	}

	public void mergeNum(IndicatorData indicatorData) {
		String query = "MERGE INTO ibt.indicatordata (indid, fordz, pop) VALUES (?,?,?);";
		jdbcTemplate.update(query, indicatorData.getIndicator().getId(), indicatorData.getDataZone().getCode(), indicatorData.getPop());

	}

	public void mergeRate(IndicatorData indicatorData) {
		String query = "MERGE INTO ibt.indicatordata (indid, fordz, rate) VALUES (?,?,?);";
		jdbcTemplate.update(query, indicatorData.getIndicator().getId(), indicatorData.getDataZone().getCode(), indicatorData.getRate());

	}

	public IndicatorData getFromDataZone(final DataZone dataZone, Indicator indicator) {
		String query = "SELECT * FROM ibt.indicatordata WHERE fordz = ? AND indid = ?;";
		IndicatorData indicatorData = jdbcTemplate.queryForObject(query, new Object[]{dataZone.getCode(), indicator.getId()}, new IndicatorDataMapper());
		return indicatorData;
	}
}
