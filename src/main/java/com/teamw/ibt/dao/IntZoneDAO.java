package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface IntZoneDAO {
	public void insert(IntZone intZone);
	public IntZone findByCode(String code);
	public List<IntZone> findByLocality(Locality locality);
}
