package com.teamw.ibt.dao;

import com.teamw.ibt.model.*;

import java.util.List;

public interface IndicatorDAO {
	public void insert(Indicator indicator);
	public List<Indicator> getAll();
	public Indicator getById(int id);
	public List<Indicator> getByCategory(Category category);
}
