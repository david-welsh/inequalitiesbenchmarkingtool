package com.teamw.ibt.model;

import java.util.ArrayList;

public class PopulationData {
	
	private int poptotal;
	private ArrayList<Integer> male;
	private ArrayList<Integer> female;
	private DataZone dataZone;		
	
	public PopulationData(DataZone dataZone, int poptotal, ArrayList<Integer> male, ArrayList<Integer> female) {
		this.dataZone = dataZone;
		this.poptotal = poptotal;
		this.male = male;
		this.female = female;
	}

	public int getPoptotal() {
		return poptotal;
	}

	public void setPoptotal(int poptotal) {
		this.poptotal = poptotal;
	}

	public ArrayList<Integer> getMale() {
		return male;
	}

	public void setMale(ArrayList<Integer> male) {
		this.male = male;
	}

	public ArrayList<Integer> getFemale() {
		return female;
	}

	public void setFemale(ArrayList<Integer> female) {
		this.female = female;
	}

	public DataZone getDataZone() {
		return dataZone;
	}

	public void setDataZone(DataZone dataZone) {
		this.dataZone = dataZone;
	}
}
