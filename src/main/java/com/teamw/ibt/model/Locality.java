package com.teamw.ibt.model;

public class Locality {

	private String name;
	private String code;
	private Region region;
	
	public Locality(String name, String code, Region region) {	
		this.name = name;
		this.code = code;
		this.region = region;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Region getRegion() {
		return region;
	}

	public void setRegion(Region region) {
		this.region = region;
	}

}
