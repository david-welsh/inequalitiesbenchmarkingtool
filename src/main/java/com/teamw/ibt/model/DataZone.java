package com.teamw.ibt.model;

public class DataZone {

	private String name;
	private String code;
	private IntZone intZone;
	
	public DataZone(String name, String code, IntZone intZone) {	
		this.name = name;
		this.code = code;
		this.intZone = intZone;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public IntZone getIntZone() {
		return intZone;
	}

	public void setIntZone(IntZone intZone) {
		this.intZone = intZone;
	}

}
