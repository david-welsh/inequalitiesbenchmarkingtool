package com.teamw.ibt.model;

public class IndicatorData {

	private Indicator indicator;
	private DataZone dataZone;
	private Double rate;
	private Integer pop;
	
	public IndicatorData(Indicator indicator, DataZone dataZone, Double rate, Integer pop) {	
		this.indicator = indicator;
		this.dataZone = dataZone;
		this.rate = rate;
		this.pop = pop;
	}

	public Indicator getIndicator() {
		return indicator;
	}

	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
	}

	public DataZone getDataZone() {
		return dataZone;
	}

	public void setDataZone(DataZone dataZone) {
		this.dataZone = dataZone;
	}

	public Double getRate() {
		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}

	public Integer getPop() {
		return pop;
	}

	public void setPop(Integer pop) {
		this.pop = pop;
	}

}
