package com.teamw.ibt.model;

public class IntZone {

	private String name;
	private String code;
	private Locality locality;
	
	public IntZone(String name, String code, Locality locality) {	
		this.name = name;
		this.code = code;
		this.locality = locality;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}

	public Locality getLocality() {
		return locality;
	}

	public void setLocality(Locality locality) {
		this.locality = locality;
	}

}
