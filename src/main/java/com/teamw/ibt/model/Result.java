package com.teamw.ibt.model;

public class Result {

	private long[] results;
	private Indicator indicator;
	
	public Result(long[] results, Indicator indicator) {	
			this.results = results;
			this.indicator = indicator;
	}

	public long[] getResults() {
		return results;
	}

	public void setResults(long[] results) {
		this.results = results;
	}

	public Indicator getIndicator() {
		return indicator;
	}

	public void setIndicator(Indicator indicator) {
		this.indicator = indicator;
	}

}
