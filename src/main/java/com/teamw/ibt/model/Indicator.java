package com.teamw.ibt.model;

public class Indicator {

	private String indicator;
	private String source;
	private Boolean rate;
	private Integer id;
	private Category category;
	private String restrict;
	
	public Indicator(String indicator, String source, Boolean rate, Integer id, Category category, String restrict) {
		this.indicator = indicator;
		this.source = source;
		this.rate = rate;
		this.id = id;
		this.category = category;
		this.restrict = restrict;
	}

	public String getIndicator() {
		return indicator;
	}

	public void setIndicator(String indicator) {
		this.indicator = indicator;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Boolean getRate() {
		return rate;
	}

	public void setRate(boolean rate) {
		this.rate = rate;
	}

	public Integer getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public Category getCategory() {
		return category;
	}

	public void setCategory(Category category) {
		this.category = category;
	}

	public String getRestrict() {
		return restrict;
	}

	public void setRestrict(String restrict) {
		this.restrict = restrict;
	}

}
