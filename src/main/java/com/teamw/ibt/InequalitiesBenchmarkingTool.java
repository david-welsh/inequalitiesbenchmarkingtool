package com.teamw.ibt;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.context.web.SpringBootServletInitializer;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

@SpringBootApplication
public class InequalitiesBenchmarkingTool extends SpringBootServletInitializer{

    @Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
	return application.sources(InequalitiesBenchmarkingTool.class);
    }	

    public static void main(String[] args) throws Exception {
    	//ApplicationContext ctx = new ClassPathXmlApplicationContext("classpath:app-context.xml");
        SpringApplication.run(InequalitiesBenchmarkingTool.class, args);
    }

}
