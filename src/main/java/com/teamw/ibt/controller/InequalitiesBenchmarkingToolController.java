package com.teamw.ibt.controller;

import com.teamw.ibt.dao.impl.*;
import com.teamw.ibt.form.*;
import com.teamw.ibt.model.*;
import com.teamw.ibt.*;

import java.io.BufferedOutputStream;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Map;
import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.multipart.MultipartFile;

@Controller
public class InequalitiesBenchmarkingToolController {

	//Initializing data accessor objects
    @Autowired
    private RegionDAOImpl regionAccess;
    @Autowired
    private LocalityDAOImpl localityAccess;
    @Autowired
    private IntZoneDAOImpl iZAccess;
    @Autowired
    private DataZoneDAOImpl dZAccess;
    @Autowired
    private PopulationDataDAOImpl pDAccess;
	@Autowired
	private IndicatorDAOImpl indicatorAccess;
	@Autowired
	private IndicatorDataDAOImpl indicatorDataAccess;
	@Autowired
	private CategoryDAOImpl categoryAccess;

    @RequestMapping(value="/", method=RequestMethod.GET)
    public String home(ModelMap model) {
		LinkedHashMap<Category, List<Indicator>> indicatorMap = new LinkedHashMap<Category, List<Indicator>>();

		List<Category> categories = categoryAccess.getAll();

		for (Category c : categories) {
			List<Indicator> indicatorList = indicatorAccess.getByCategory(c);
			indicatorMap.put(c, indicatorList);
		}

		Benchmarking benchmarking = new Benchmarking();
		model.addAttribute("benchmarkingForm",benchmarking);

		// Generate hierarchical map for geography info for front page
		LinkedHashMap<Region, LinkedHashMap<Locality, LinkedHashMap<IntZone, List<DataZone>>>> geoMap 
						= new LinkedHashMap<Region, LinkedHashMap<Locality, LinkedHashMap<IntZone, List<DataZone>>>>();

		List<Region> regionList = regionAccess.getAll();
		for (Region r : regionList) {
			LinkedHashMap<Locality, LinkedHashMap<IntZone, List<DataZone>>> localityMap 
							= new LinkedHashMap<Locality, LinkedHashMap<IntZone, List<DataZone>>>();
			for (Locality l : localityAccess.findByRegion(r)) {
				LinkedHashMap<IntZone, List<DataZone>> iZMap = new LinkedHashMap<IntZone, List<DataZone>>();
				for (IntZone i : iZAccess.findByLocality(l)) {
					iZMap.put(i, dZAccess.findByIntZone(i));
				}
				localityMap.put(l, iZMap);
			}
			geoMap.put(r, localityMap);
		}

		model.addAttribute("geoMap", geoMap);
		
		model.addAttribute("indMap", indicatorMap);

        return "index";
    }   

    @RequestMapping(value="/error", method=RequestMethod.POST)
    public String error(ModelMap model){
    	return "error";
    }
    

    @RequestMapping(value="/about", method=RequestMethod.GET)
    public String about(ModelMap model) {
        return "about";
    }

	@RequestMapping(value="/method", method=RequestMethod.GET)
	public String method(ModelMap model) {
		return "method";
	}

    @RequestMapping(value="/results", method=RequestMethod.POST)
	public  String home(@ModelAttribute Benchmarking benchmarking,ModelMap model,BindingResult bindingResult) {
		if(bindingResult.hasErrors()){
			return "index";
		}

		String areaName;

		List<DataZone> dataZones;

		// Process the geography input, either region, locality, intzone or single datazone
		// Gives list of DataZone objects
		if (benchmarking.getDatazone().equals("")) {
			if (benchmarking.getIntzone().equals("")) {
				if (benchmarking.getLocality().equals("")) {
					Region region = regionAccess.findByCode(benchmarking.getRegion());
					dataZones = dZAccess.getInRegion(region);
					areaName = region.getName();
				} else {
					Locality locality = localityAccess.findByCode(benchmarking.getLocality());
					dataZones = dZAccess.getInLocality(locality);
					areaName = locality.getName();
				}
			} else {
				IntZone intZone = iZAccess.findByCode(benchmarking.getIntzone());
				dataZones = dZAccess.getInIntZone(intZone);
				areaName = intZone.getName();
			}
		} else {
			dataZones = new ArrayList<DataZone>();
			DataZone dataZone = dZAccess.findByCode(benchmarking.getDatazone());
			dataZones.add(dataZone);
			areaName = dataZone.getName();
		}

		HashMap<DataZone, PopulationData> dataMap = new HashMap<DataZone, PopulationData>();

		for (DataZone dz : dataZones) {
			PopulationData pd = pDAccess.findByDZ(dz);
			dataMap.put(dz, pd);
		}

		int finalTotalPop = 0;
		for (DataZone key : dataMap.keySet()) {
			finalTotalPop += dataMap.get(key).getPoptotal();
		}

		// Get list of ages specified by user
		ArrayList<Integer> ages = new ArrayList<Integer>();
		for(String ageRange : benchmarking.getAge().split(",")) {
			int min;
			int max;
			if (ageRange.charAt(2) == '+'){
				min = 65;
				max = 200;
			} else {
				String[] ageRangeSplit = ageRange.split("-");
				min = Integer.parseInt(ageRangeSplit[0]);
				max = Integer.parseInt(ageRangeSplit[1]);
			}
			for (; min <= max; min++) {
				ages.add(min);
			}
		}
		Collections.sort(ages);

		// Get M or F for sex specified by user
		String formSex = benchmarking.getSex().charAt(0) + "";
		formSex = formSex.toUpperCase();

		ArrayList<Result> resultList = new ArrayList<Result>();
		for (String s : benchmarking.getDepIndicator().split(",")) {
			Indicator ind = indicatorAccess.getById(Integer.parseInt(s));

			HashMap<DataZone, IndicatorData> indicatorDataMap = new HashMap<DataZone, IndicatorData>();
			for (DataZone dz : dataZones) {
				IndicatorData indData = indicatorDataAccess.getFromDataZone(dz, ind);
				indicatorDataMap.put(dz, indData);
			}

			long[] result;

			// Check if indicator is restricted by sex/age and adjust results accordingly
			if (ind.getRestrict() != null) {
				String[] restrict = ind.getRestrict().split(",");
				boolean ageContained = false;
				for (int i = Integer.parseInt(restrict[1]); i <= Integer.parseInt(restrict[2]); i++) {
					if (ages.contains(i)) {
						ageContained = true;
						break;
					}
				}
				if (!((restrict[0].equals(formSex) || restrict[0].equals("N")) && ageContained)) {
					result = new long[]{0L,0L,0L};
					Result newResult = new Result(result, ind);
					resultList.add(newResult);
					continue;
				}
			}

			if (ind.getRate()) {
				Double finalRate = 0.0;
				int i = 0;
				for (DataZone key : indicatorDataMap.keySet()) {
					finalRate += indicatorDataMap.get(key).getRate();
					i++;
				}
				finalRate = finalRate / i;
				result = Benchmarker.benchmarkRate(finalRate, Integer.parseInt(benchmarking.getNumberOfPeople()));
			} else {
				int finalDeprivedPop = 0;
				for (DataZone key : indicatorDataMap.keySet()) {
					finalDeprivedPop += indicatorDataMap.get(key).getPop();
				}
   	     		result = Benchmarker.benchmarkPopulation(finalTotalPop, finalDeprivedPop,
                        	Integer.parseInt(benchmarking.getNumberOfPeople()));
			}
			Result newResult = new Result(result, ind);
			resultList.add(newResult);
		}

		model.addAttribute("benchmarkingForm",benchmarking);

        model.addAttribute("groupPop", benchmarking.getNumberOfPeople());
        model.addAttribute("sex",benchmarking.getSex());
        model.addAttribute("ageGroup",benchmarking.getAge());
        model.addAttribute("dataZoneName", areaName);
        model.addAttribute("dataZonePop", finalTotalPop);
		model.addAttribute("results", resultList);

        return "results";
    }


/*
 * ----------------------------------------------------------------------------
 * 								Admin Pages
 * ----------------------------------------------------------------------------
 */

	@RequestMapping(value="/admin", method=RequestMethod.GET)
	public String adminPage(ModelMap model) {
		IndicatorForm indicatorForm = new IndicatorForm();
		ImportForm importForm = new ImportForm();
		AddRateForm addRateForm = new AddRateForm();

		List<Category> categories = categoryAccess.getAll();
		List<Indicator> indicators = indicatorAccess.getAll();

		model.addAttribute("indicatorForm", indicatorForm);
		model.addAttribute("importForm", importForm);
		model.addAttribute("addRateForm", addRateForm);

		model.addAttribute("categories", categories);
		model.addAttribute("indicators", indicators);

		return "admin";
	}

    @RequestMapping(value="/import", method=RequestMethod.POST)
	public  String importPagePost(@ModelAttribute ImportForm importForm,ModelMap model) {
		MultipartFile file = importForm.getFile();
		if (!file.isEmpty()) {
			try {
				byte[] bytes = file.getBytes();
				File newFile = new File("./static/" + file.getOriginalFilename());
				BufferedOutputStream stream = new BufferedOutputStream(new FileOutputStream(newFile));
				stream.write(bytes);
				stream.close();

				try {
					BufferedReader csvBuff = new BufferedReader(new FileReader(newFile));

					String ln = csvBuff.readLine();
					String[] headers = ln.split(",");

					ArrayList<String[]> tenLines = new ArrayList<String[]>();

					for (int i = 0; i < 10; i++) {
						ln = csvBuff.readLine();
                        //Splits csv line, with support for quotation marks containing commas
						tenLines.add(ln.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)"));
					}

					List<Indicator> indicators = indicatorAccess.getAll();

					model.addAttribute("headers", headers);
					model.addAttribute("tenLines", tenLines);
					model.addAttribute("indicators", indicators);
					model.addAttribute("file", newFile);
					ImportProcForm importProcForm = new ImportProcForm();
					model.addAttribute("importProcForm", importProcForm);
				} catch (Exception e) {
				}
			} catch (Exception e) {
			}
		}
		return "importproc";
	}

	@RequestMapping(value="/add-indicator", method=RequestMethod.POST)
	public String addIndicatorAcceptPage(@ModelAttribute IndicatorForm indicatorForm, ModelMap model) {
		String restrict = null;
		if (indicatorForm.getSex() != null || !indicatorForm.getMinAge().equals("") || !indicatorForm.getMaxAge().equals("")) {
			if (indicatorForm.getSex() != null) {
				restrict = indicatorForm.getSex() + ",";
			} else {
				restrict = "N,";
			}

			if (!indicatorForm.getMinAge().equals("")) {
				restrict += indicatorForm.getMinAge() + ",";
			} else {
				restrict += "0,";
			}
			if (!indicatorForm.getMaxAge().equals("")) {
				restrict += indicatorForm.getMaxAge();
			} else {
				restrict += "200";
			}
		}
		Indicator indicator = new Indicator(indicatorForm.getIndicatorName(),
											indicatorForm.getSource(),
											indicatorForm.getRate(),
											null,
											categoryAccess.getByName(indicatorForm.getCategory()),
											restrict
											);
		indicatorAccess.insert(indicator);
		model.addAttribute("indName", indicator.getIndicator());
		return "add-indicator-accept";
	}

	@RequestMapping(value="/import/{filename}", method=RequestMethod.POST)
	public String importFileProcessing(@PathVariable String filename, ModelMap model,
                                       @ModelAttribute ImportProcForm importProcForm) {
		int datazoneColumn = importProcForm.getDzColumn();
		int indicatorColumn = importProcForm.getIndColumn();
		int indicatorId = importProcForm.getIndId();

		try {
			BufferedReader csvBr = new BufferedReader(new FileReader("./static/" + filename));
			
			// Skip header
			String line = csvBr.readLine();

			line = csvBr.readLine();

			while (line != null) {
				String[] lineSplit = line.split(",");
				Indicator ind = indicatorAccess.getById(indicatorId);
				DataZone dz = dZAccess.findByCode(lineSplit[datazoneColumn]);

				if (dz != null) {
					IndicatorData newData = new IndicatorData(ind, dz, null, 
							Integer.parseInt(lineSplit[indicatorColumn]));

					indicatorDataAccess.mergeNum(newData);
				}
				line = csvBr.readLine();
			}

		} catch (Exception e) {
			return "redirect:/error";
		}


		model.addAttribute("file", filename);

		return "importing";
	}

	@RequestMapping(value="/add-rate", method=RequestMethod.POST)
	public String addRatePost(@ModelAttribute AddRateForm addRateForm, ModelMap model) {
		Indicator indicator = indicatorAccess.getById(addRateForm.getIndicator());
		IndicatorData indicatorData;
		List<DataZone> datazones = dZAccess.getAll();
		for (DataZone d : datazones) {
			indicatorData = new IndicatorData(indicator, d, addRateForm.getRate()/100f, null);
			indicatorDataAccess.mergeRate(indicatorData);
		}
		model.addAttribute("rate", addRateForm.getRate());
		model.addAttribute("indName", indicator.getIndicator());
		return "added-rate";
	}

/*
 * ----------------------------------------------------------------------------
 */

}


