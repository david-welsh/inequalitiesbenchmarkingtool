package com.teamw.ibt.form;

import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;

public class Benchmarking{

@NotNull
@Min(0)
private String numberOfPeople;
@NotNull
private String sex;
@NotNull
private String age;
@NotNull
private String depIndicator; 

private String datazone; 
private String intzone;
private String locality;
private String region;


public String getNumberOfPeople(){
	return numberOfPeople;
}

public String getSex(){
	return sex;
}

public String getAge(){
	return age;
}

public String getDepIndicator(){
	return depIndicator;
}

public String getDatazone(){
	return datazone;
}
public String getIntzone(){
	return intzone;
}

public String getLocality(){
	return locality;
}

public String getRegion(){
	return region;
}

public void setNumberOfPeople(String numberOfPeople){
	this.numberOfPeople=numberOfPeople;
}

public void setSex(String sex){
	this.sex = sex;
}

public void setAge(String age){
	this.age=age;
}

public void setDepIndicator(String depIndicator){
	this.depIndicator=depIndicator;
}


public void setDatazone(String datazone){
	this.datazone=datazone;
}

public void setIntzone(String intzone){
	this.intzone=intzone;
}

public void setLocality(String locality){
	this.locality=locality;
}

public void setRegion(String region){
	this.region=region;
}

public String toString()
{
return "NumberOfPeople";
}
}
