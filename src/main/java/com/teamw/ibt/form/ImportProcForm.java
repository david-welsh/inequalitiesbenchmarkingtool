package com.teamw.ibt.form;

public class ImportProcForm {
	private Integer indId;
	private Integer dzColumn;
	private Integer indColumn;

	public Integer getIndId() {
		return indId;
	}

	public void setIndId(Integer indId) {
		this.indId = indId;
	}

	public Integer getDzColumn() {
		return dzColumn;
	}

	public void setDzColumn(Integer dzColumn) {
		this.dzColumn = dzColumn;
	}

	public Integer getIndColumn() {
		return indColumn;
	}

	public void setIndColumn(Integer indColumn) {
		this.indColumn = indColumn;
	}

}
