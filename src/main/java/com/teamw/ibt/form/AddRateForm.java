package com.teamw.ibt.form;

public class AddRateForm {
	private Integer indicator;
	private Double rate;

	public Integer getIndicator() {
		return indicator;
	}

	public void setIndicator(Integer indicator) {
		this.indicator = indicator;
	}

	public Double getRate() {

		return rate;
	}

	public void setRate(Double rate) {
		this.rate = rate;
	}
}
