package com.teamw.ibt.form;

import org.springframework.web.multipart.MultipartFile;

public class ImportForm {
	private MultipartFile file;
	private Boolean fileUsed;

	public MultipartFile getFile() {
		return file;
	}

	public void setFile(MultipartFile file) {
		this.file = file;
	}

	public Boolean getFileUsed() {

		return fileUsed;
	}

	public void setFileUsed(boolean fileUsed) {
		this.fileUsed = fileUsed;
	}
}
