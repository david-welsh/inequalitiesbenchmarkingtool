//based on http://jsfiddle.net/datashaman/rBfy5/4/light/
var first = new Array();
var second = new Array();
var third = new Array();

//gets the data from the table
for(var j=2, row; row=table.rows[j]; j++) {
  var min = parseInt(row.cells[1].innerHTML);
  var expected = parseInt(row.cells[2].innerHTML);
  var max = parseInt(row.cells[3].innerHTML);
  var depInd = row.cells[0].innerHTML;
  first.push({numbers: min, indicators:depInd});
  second.push({numbers:expected, indicators: depInd});
  third.push({ numbers:max, indicators: depInd});

}
var gender = document.getElementById("sex").innerHTML;
var ageG = document.getElementById("age").innerHTML;
var title = "Results for ".concat(gender).concat(" in the ").concat(ageG).concat(" age group");

var lengthData = first.length;
console.log(lengthData);


var margins = {
    top: 12,
    left: 80,
    right: 24,
    bottom: 24
},
legendPanel = {
    width: 180
},
width = 500 + margins.left - margins.right - legendPanel.width,
    height = 90 - margins.top - margins.bottom,
    dataset = [{
        data:first,
        name: 'lower confidence interval'
    }, {
        data:second,
        name: 'expected'
    },{
        data:third,
        name: 'upper confidence interval'
    }

    ],
    series = dataset.map(function (d) {
        return d.name;
    }),
    dataset = dataset.map(function (d) {
        return d.data.map(function (o, i) {
            // Structure it so that your numeric
            // axis (the stacked amount) is y
            return {
                y: o.numbers,
                x: o.indicators
            };
        });
    }),
    stack = d3.layout.stack();

stack(dataset);
var dataset = dataset.map(function (group) {
    return group.map(function (d) {
        // Invert the x and y values, and y0 becomes x0
        return {
            x: d.y,
            y: d.x,
            x0: d.y0
        };
    });
}),

    svg = d3.select('#tooltip')
        .append('svg')
        .attr('width', 1000+ legendPanel.width)
        .attr('height',height*lengthData + 200)
        .attr('align','center')
        .append('g')
        .attr('transform', 'translate(' + 600+ ',' + 90 + ')'),
    xMax = d3.max(dataset, function (group) {
        return d3.max(group, function (d) {
            return d.x + d.x0;
        });
    }),
    xScale = d3.scale.linear()
        .domain([0, xMax])
        .range([0, width])

    depIndicators = dataset[0].map(function (d) {
        return d.y;
    }),
    yScale = d3.scale.ordinal()
        .domain(depIndicators)
        .rangeRoundBands([0,(lengthData*height)/2], .1),
    xAxis = d3.svg.axis()
        .scale(xScale)
        .ticks(4)
        .orient('bottom'),
    yAxis = d3.svg.axis()
        .scale(yScale)
        .orient('left'),
    colours = d3.scale.category10(),
    groups = svg.selectAll('g')
        .data(dataset)
        .enter()
        .append('g')
        .style('fill', function (d, i) {
        return colours(i);
    }),    rects = groups.selectAll('rect')
        .data(function (d) {
        return d;
    })
        .enter()
        .append('rect')
        .attr('x', function (d) {
        return xScale(d.x0);
    })
        .attr('y', function (d, i) {
        return yScale(d.y);
    })
        .attr('height', function (d) {
        return yScale.rangeBand();
    })
        .attr('width', function (d) {
        return xScale(d.x);
    })
    svg.append('g')
        .attr('class', 'axis')
        .attr('transform', 'translate(0,' + (lengthData*height)/2+ ')')
        .call(xAxis);

svg.append('g')
    .attr('class', 'axis')
    .call(yAxis);

svg.append('rect')
    .attr('fill', 'transparent')
    .attr('width', 60)
    .attr('height', 30 * dataset.length)
    .attr('transform', 'translate(' +300  + ')')


    series.forEach(function (s, i) {
    svg.append('text')
        .attr('fill', 'black')
        .attr('x', margins.left -540)
        .attr('y', i * 24 + 24)
        .text(s);
    svg.append('rect')
        .attr('fill', colours(i))
        .attr('width', 60)
        .attr('height', 20)
        .attr('x', margins.left -620)
        .attr('y', i * 24 + 6);


    svg.append("text")
            .attr("text-anchor", "middle")  // this makes it easy to centre the text as the transform is applied to the anchor
            .attr("transform", "translate("+ (width/2) +","+((lengthData*height)/2+40)+")")  // centre below axis
            .text("Number of People");
  svg.append("text")
        .attr("x", 50)
        .attr("y", 0 - (margins.top / 2))
        .attr("text-anchor", "middle")  
        .style("font-size", "16px") 
        .style("text-decoration", "underline")  
        .text(title);
});


function showChart(){
    if(document.getElementById("tooltip").style.display == "none"){
        document.getElementById("tooltip").style.display = "block";
        document.getElementById("chartButton").innerHTML = "Hide Stacked Bar Chart";
    }
    else {
        document.getElementById("tooltip").style.display = "none";
        document.getElementById("chartButton").innerHTML = "Show Stacked Bar Chart";
    }
}