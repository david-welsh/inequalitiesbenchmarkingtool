import java.sql.*;

public class DatabaseConnector {
	private static final String DATABASECONNECTION = "jdbc:h2:./data/ibt.db;DB_CLOSE_ON_EXIT=FALSE";
	private static final String CREATEDATABASE = 
		"CREATE SCHEMA IBT;" + 
		"CREATE TABLE IF NOT EXISTS ibt.region" +
		" (code			VARCHAR	NOT NULL UNIQUE," +
		" name			VARCHAR	NOT NULL UNIQUE," +
		" PRIMARY KEY(code));" +
		"CREATE TABLE IF NOT EXISTS ibt.locality" +
		" (name			VARCHAR	NOT NULL UNIQUE," +
		" code			VARCHAR	NOT NULL UNIQUE," +
		" inregion		VARCHAR	NOT NULL," +
		" PRIMARY KEY(code)," +
		" FOREIGN KEY(inregion) REFERENCES region(code));" +
		"CREATE TABLE IF NOT EXISTS ibt.intzone" +
		" (name			VARCHAR	NOT NULL UNIQUE," +
		" code			VARCHAR	NOT NULL UNIQUE," +
		" inlocality		VARCHAR	NOT NULL," +
		" PRIMARY KEY(code)," +
		" FOREIGN KEY(inlocality) REFERENCES locality(code));" +
		"CREATE TABLE IF NOT EXISTS ibt.datazone" +
		" (name			VARCHAR	NOT NULL UNIQUE," +
		" code			VARCHAR	NOT NULL UNIQUE," +
		" inintzone	     	VARCHAR	NOT NULL," +
		" PRIMARY KEY(code)," +
		" FOREIGN KEY(inintzone) REFERENCES intzone(code));" +
		"CREATE TABLE IF NOT EXISTS ibt.populationdata" +
		" (poptotal			INT," +
		" pop0to4f			INT," +
		" pop0to4m			INT," +
		" pop5to9f			INT," +
		" pop5to9m			INT," +
		" pop10to14f		INT," +
		" pop10to14m		INT," +
		" pop15to19f		INT," +
		" pop15to19m		INT," +
		" pop20to24f		INT," +
		" pop20to24m		INT," +
		" pop25to29f		INT," +
		" pop25to29m		INT," +
		" pop30to34f		INT," +
		" pop30to34m		INT," +
		" pop35to39f		INT," +
		" pop35to39m		INT," +
		" pop40to44f		INT," +
		" pop40to44m		INT," +
		" pop45to49f		INT," +
		" pop45to49m		INT," +
		" pop50to54f		INT," +
		" pop50to54m		INT," +
		" pop55to59f		INT," +
		" pop55to59m		INT," +
		" pop60to64f		INT," +
		" pop60to64m		INT," +
		" pop65to69f		INT," +
		" pop65to69m		INT," +
		" pop70to74f		INT," +
		" pop70to74m		INT," +
		" pop75to79f		INT," +
		" pop75to79m		INT," +
		" pop80to84f		INT," +
		" pop80to84m		INT," +
		" pop85to89f		INT," +
		" pop85to89m		INT," +
		" pop90plusf		INT," +
		" pop90plusm		INT," +
		" fordz			VARCHAR	NOT NULL UNIQUE," +
		" PRIMARY KEY(fordz)," +
		" FOREIGN KEY(fordz) REFERENCES datazone(code));" + 
		"CREATE TABLE IF NOT EXISTS ibt.indicator" +
		"(indicatorname		VARCHAR(128)," +
		" indid				INT		AUTO_INCREMENT," +
		" source			VARCHAR(255)," +
		" rate				BOOLEAN," +
		" PRIMARY KEY(indid));" +
		"CREATE TABLE IF NOT EXISTS ibt.indicatordata" +
		"(indid				INT," +
		" fordz				VARCHAR," +
		" rate				DOUBLE," +
		" pop				INT," +
		" PRIMARY KEY(indid, fordz)," +
		" FOREIGN KEY(indid) REFERENCES indicator(indid)," +
		" FOREIGN KEY(fordz) REFERENCES datazone(code));";

	private static final String ADDREGION = 
		"MERGE INTO ibt.region (name, code) VALUES ('%s', '%s');";
	
	private static final String GETREGION = 
		"SELECT code FROM ibt.region WHERE name = '%s';";

	private static final String ADDLOCALITY = 
		"MERGE INTO ibt.locality (name, code, inregion) VALUES ('%s', '%s', '%s');";
	
	private static final String GETLOCALITY =
		"SELECT code FROM ibt.locality WHERE name = '%s';";
 
	private static final String ADDINTZONE = 
		"MERGE INTO ibt.intzone (name, code, inlocality) VALUES ('%s', '%s', '%s');";
	
	private static final String GETINTZONE =
		"SELECT code FROM ibt.intzone WHERE name = '%s';";

	private static final String ADDDATAZONE = 
		"MERGE INTO ibt.datazone (name, code, inintzone) VALUES ('%s', '%s', '%s');";
	
	private static final String GETDATAZONE =
		"SELECT code FROM datazone WHERE name = '%s';";

	private static final String ADDPOPDATA = 
		"MERGE INTO ibt.populationdata (poptotal" +
		", pop0to4f,   pop0to4m,   pop5to9f,   pop5to9m" +
		", pop10to14f, pop10to14m, pop15to19f, pop15to19m" +
		", pop20to24f, pop20to24m, pop25to29f, pop25to29m" +
		", pop30to34f, pop30to34m, pop35to39f, pop35to39m" +
		", pop40to44f, pop40to44m, pop45to49f, pop45to49m" +
		", pop50to54f, pop50to54m, pop55to59f, pop55to59m" +
		", pop60to64f, pop60to64m, pop65to69f, pop65to69m" +
		", pop70to74f, pop70to74m, pop75to79f, pop75to79m" +
		", pop80to84f, pop80to84m, pop85to89f, pop85to89m" +
		", pop90plusf, pop90plusm" +
		", fordz) VALUES (%d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d, %d, %d" +
		", %d, %d" +
		", '%s');";
	
	private static final String GETPOPDATA = 
		"SELECT * FROM ibt.populationdata WHERE fordz = '%s';";

	private static final String UPDATEPOP = 
		"UPDATE ibt.populationdata SET pop = %d WHERE fordz = '%s';";

	private static final String ADDINDICATOR =
		"MERGE INTO ibt.indicator (indicator, source, rate) VALUES ('%s', '%s', %b);";
	
	private static final String ADDINDICATORDATAPOP =
		"MERGE INTO ibt.indicatordata (indid, fordz, pop) VALUES (%d, '%s', %d);";

	private static final String ADDINDICATORDATARATE =
		"MERGE INTO ibt.indicatordata (indid, fordz, rate) VALUES (%d, '%s', %f);";

	private Connection conn;
		
	public DatabaseConnector() {
		try {
			Class.forName("org.h2.Driver");
			conn = DriverManager.getConnection(DATABASECONNECTION);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void createDatabase() {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(CREATEDATABASE);
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void close() {
		try {
			conn.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addRegion(String name, String code) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(String.format(ADDREGION, name, code));
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addLocality(String name, String code, String region) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(String.format(ADDLOCALITY, name, code, region));
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}	
	
	public void addIntzone(String name, String code, String locality) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(String.format(ADDINTZONE, name, code, locality));
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addDatazone(String code, String name, String intzone, Integer[] population) {
		Statement stmt;
		Statement getstmt;
		try {
			stmt = conn.createStatement();
			getstmt = conn.createStatement();
			stmt.executeUpdate(String.format(ADDDATAZONE, name, code, intzone));
			stmt.executeUpdate(String.format(ADDPOPDATA, population[0]
				, population[1], population[2], population[3], population[4]
				, population[5], population[6], population[7], population[8]
				, population[9], population[10], population[11], population[12]
				, population[13], population[14], population[15], population[16]
				, population[17], population[18], population[19], population[20]
				, population[21], population[22], population[23], population[24]
				, population[25], population[26], population[27], population[28]
				, population[29], population[30], population[31], population[32]
				, population[33], population[34], population[35], population[36]
				, population[37], population[38]
				, code));
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addIndicator(String indString, String indSource, boolean rate) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			stmt.executeUpdate(String.format(ADDINDICATOR, indString, indSource, rate));
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	public void addIndicatorData(int indID, String fordz, double rate, int pop) {
		Statement stmt;
		try {
			stmt = conn.createStatement();
			if (rate == null) {
				stmt.executeUpdate(String.format(ADDINDICATORDATAPOP, indID, fordz, pop);
			} else {
				stmt.executeUpdate(String.format(ADDINDICATORDATARATE, indID, fordz, rate);
			}
			stmt.close();
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
}
