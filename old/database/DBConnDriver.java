import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Scanner;

public class DBConnDriver {
	private static final String USAGE = "java DBConnDriver [geofile.csv] [popfile.csv] [datazones.csv]";

	public static void main(String[] args) {
		if (args.length != 3) {
			System.err.println(USAGE);
			System.exit(-1);
		}

		BufferedReader geobr;
		BufferedReader popbr;
		BufferedReader dataZoneBr;
		Scanner input = new Scanner(System.in);
		HashMap<String, Integer[]> popLookup = new HashMap<String, Integer[]>();
		HashMap<String, String> dzLookup = new HashMap<String, String>();

		try {
			popbr = new BufferedReader(new FileReader(args[1]));
			
			String ln = popbr.readLine();

			Integer[] popColumns = new Integer[3];

			String[] popLineSplit = ln.split(",");
			String[] popInput = new String[] {
				"Datazone",
				"Total population",
				"Population 0-4"
			};

			for (int i = 0; i < popLineSplit.length; i++) {
				System.out.println(i + " - " + popLineSplit[i]);
			}
		
			for (int i = 0; i < popInput.length; i++) {
				System.out.print(popInput[i] + ": ");
				popColumns[i] = input.nextInt();
			}
	
			while ((ln = popbr.readLine()) != null) {
				String nextLine = popbr.readLine();
				String[] data1 = ln.split(",");
				String[] data2 = nextLine.split(",");
				popLookup.put(data1[popColumns[0]], new Integer[] {
					Integer.parseInt(data1[popColumns[1]]) + Integer.parseInt(data2[popColumns[1]]),
					Integer.parseInt(data2[popColumns[2]]), Integer.parseInt(data1[popColumns[2]]), Integer.parseInt(data2[popColumns[2] + 1]), Integer.parseInt(data1[popColumns[2] + 1]),
					Integer.parseInt(data2[popColumns[2] + 2]), Integer.parseInt(data1[popColumns[2] + 2]), Integer.parseInt(data2[popColumns[2] + 3]), Integer.parseInt(data1[popColumns[2] + 3]),
					Integer.parseInt(data2[popColumns[2] + 4]), Integer.parseInt(data1[popColumns[2] + 4]), Integer.parseInt(data2[popColumns[2] + 5]), Integer.parseInt(data1[popColumns[2] + 5]),
					Integer.parseInt(data2[popColumns[2] + 6]), Integer.parseInt(data1[popColumns[2] + 6]), Integer.parseInt(data2[popColumns[2] + 7]), Integer.parseInt(data1[popColumns[2] + 7]),
					Integer.parseInt(data2[popColumns[2] + 8]), Integer.parseInt(data1[popColumns[2] + 8]), Integer.parseInt(data2[popColumns[2] + 9]), Integer.parseInt(data1[popColumns[2] + 9]),
					Integer.parseInt(data2[popColumns[2] + 10]), Integer.parseInt(data1[popColumns[2] + 10]), Integer.parseInt(data2[popColumns[2] + 11]), Integer.parseInt(data1[popColumns[2] + 11]),
					Integer.parseInt(data2[popColumns[2] + 12]), Integer.parseInt(data1[popColumns[2] + 12]), Integer.parseInt(data2[popColumns[2] + 13]), Integer.parseInt(data1[popColumns[2] + 13]),
					Integer.parseInt(data2[popColumns[2] + 14]), Integer.parseInt(data1[popColumns[2] + 14]), Integer.parseInt(data2[popColumns[2] + 15]), Integer.parseInt(data1[popColumns[2] + 15]),
					Integer.parseInt(data2[popColumns[2] + 16]), Integer.parseInt(data1[popColumns[2] + 16]), Integer.parseInt(data2[popColumns[2] + 17]), Integer.parseInt(data1[popColumns[2] + 17]),
					Integer.parseInt(data2[popColumns[2] + 18]), Integer.parseInt(data1[popColumns[2] + 18])
				});
			}

			dataZoneBr = new BufferedReader(new FileReader(args[2]));

			String dataZoneLine = dataZoneBr.readLine();

			while ((dataZoneLine = dataZoneBr.readLine()) != null) {
				String[] lineSplit = dataZoneLine.split(",");
				dzLookup.put(lineSplit[0], lineSplit[1]);
			}


			geobr = new BufferedReader(new FileReader(args[0]));

			DatabaseConnector c = new DatabaseConnector();
			c.createDatabase();

			int regionCode;
			int regionName;
			int localityCode;
			int localityName;
			int intzoneCode;
			int intzoneName;
			int datazoneCode;
			Integer[] geoColumns = new Integer[7];

			String[] geoInput = new String[] {
				"Region code",
				"Region name",
				"Locality code",
				"Locality name",
				"Intermediate Zone code",
				"Intermediate Zone name",
				"Datazone code"
			};

			String line = geobr.readLine(); //Skip first line
			String[] geoLineSplit = line.split(",");

			for (int i = 0; i < geoLineSplit.length; i++) {
				System.out.println(i + " - " + geoLineSplit[i]);
			}

			for (int i = 0; i < geoInput.length; i++) {
				System.out.print(geoInput[i] + ": ");
				geoColumns[i] = input.nextInt();
			}

			while ((line = geobr.readLine()) != null) {
				String[] data = line.split(",");
				boolean skip = false;

				for (int i = 0; i < geoColumns.length; i++) {
					if (data[geoColumns[i]].matches("\\s+") || data[geoColumns[i]].equals("") ) {
						skip = true;
						break;
					}
				}
				
				if (!skip) {
					c.addRegion(data[geoColumns[1]], data[geoColumns[0]]);
					c.addLocality(data[geoColumns[3]], data[geoColumns[2]], data[geoColumns[0]]);
					c.addIntzone(data[geoColumns[5]], data[geoColumns[4]], data[geoColumns[2]]);
					c.addDatazone(data[geoColumns[6]], dzLookup.get(data[geoColumns[6]]) ,data[geoColumns[4]], popLookup.get(data[geoColumns[6]]));

					System.out.println("Added " + data[geoColumns[6]]);
				}
			}

			geobr.close();	
			c.close();
		} catch (Exception e) {
			e.printStackTrace();	
		}

	}
}
