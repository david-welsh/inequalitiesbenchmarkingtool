public class BenchmarkerDriver {
	public static void main(String[] args) {
		long[] r = Benchmarker.benchmark(Integer.parseInt(args[0]), Integer.parseInt(args[1]), Integer.parseInt(args[2]));

		System.out.println("engaged people " + r[0]);
		System.out.println("lower limit " + r[1]);
		System.out.println("upper limit " + r[2]);
	}
}
