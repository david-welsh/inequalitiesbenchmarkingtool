import static org.junit.Assert.assertEquals;
import org.junit.Test;

public class BenchmarkerTest {
	
	@Test
	public void correctResults() {
		long[] results = Benchmarker.benchmark(23990, 3628, 586);
		assertEquals(results[0], 89);
		assertEquals(results[1], 73);
		assertEquals(results[2], 107);	
	}	

	@Test
	public void zeroGroupSize() {
		long[] results = Benchmarker.benchmark(23990, 3628, 0);
		assertEquals(results[0], 0);
		assertEquals(results[1], 0);
		assertEquals(results[2], 0);
	}

	@Test
	public void groupSizeSameAsPopulation() {
		long[] results = Benchmarker.benchmark(23990, 3628, 23990);
		assertEquals(results[0], 3628);
		assertEquals(results[1], 3521);
		assertEquals(results[2], 3738);	
	}

}
