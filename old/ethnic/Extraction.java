package src;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class Extraction {

  public static void main(String[] args) {
	File folder = new File(System.getProperty("user.dir") + "/csv/SNS Data Zone 2011 std/");
	Extraction obj = new Extraction();
	
	Writer CSVwriter = new Writer();
	for (File file : folder.listFiles()) {
		String csvFile = file.getAbsolutePath();
		obj.extractAndWrite(csvFile, CSVwriter);
	}
	System.out.println("Finished!");
  }

public void extractAndWrite(String csvFile, Writer CSVwriter) {

	BufferedReader br = null;
	String line = "";
	String cvsSplitBy = ",";
	List<String[]> data = new LinkedList<String[]>();
	String fileToWrite = "Error Naming File";
	int i = 0;
	
	try {
		br = new BufferedReader(new FileReader(csvFile));
		while ((line = br.readLine()) != null) {
			if(line.isEmpty() ){
				continue;
			} else if (line.charAt(0) != '\"'){
				if (line.charAt(0) == ','){
					line = line.replace("\"", "");
					String[] headers = line.split(cvsSplitBy);
					if (headers == null){
						continue;
					}
					headers[0] = "Zone";
					data.add(headers);
				} else if (line.startsWith("Table")){
					fileToWrite = line;
					System.out.println(fileToWrite);
					continue;
				}
				continue;
			}
			
			line = line.replace("\"", "");
			
			if (i == 0){
				String[] headers = line.split(cvsSplitBy);
				data.add(headers);
				i++;
				continue;
			}
			
			String[] CSVdata = line.split(cvsSplitBy);
			
			for (int j = 0; j< CSVdata.length; j++){
				if (CSVdata[j].equals("-")){
					CSVdata[j] = "0";
				}
			}
			
			if (CSVdata[0].compareTo("S01007481") >= 0 && CSVdata[0].compareTo("S01007681") <= 0) {
				data.add(CSVdata);
			}
		}

	} catch (FileNotFoundException e) {
		e.printStackTrace();
	} catch (IOException e) {
		e.printStackTrace();
	} finally {
		if (br != null) {
			try {
				br.close();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}
	CSVwriter.generateCsvFile(fileToWrite, data);
//	for (String[] s : data){
//		System.out.println(Arrays.toString(s));
//	}
//	System.out.println("Done");
  }

}