package src;

import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class Writer {

	public Writer() {
	}

	public void generateCsvFile(String sFileName, List<String[]> data) {
		try {
			FileWriter writer = new FileWriter(sFileName);

			for (String[] array : data) {
				for (int i = 0; i < array.length; i++) {
					writer.append(array[i]);
					writer.append(",");
					if (i == array.length - 1) {
						writer.append("\n");
					}
				}
			}
			writer.flush();
			writer.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}
}