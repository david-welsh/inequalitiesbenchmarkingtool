Third year Team Project
-----------------------
This is a web application I developed along with a team of 5 other students



Inequalities Benchmarking Tool
Version:1.0    Data: March 25th,2016

What is it?
------------
A web application that allows service providers to estimate the number of deprived
people in their group. The information is presented in a well-structured table and
the user has the option to export the results as csv or pdf file. There is also a d3 
chart that displays the results. The admin can add new deprivation indicators and 
add/edit the data. 

Algorithm Used
--------------
The predicted number of people in a deprived category for a specific subset of the 
population of Dumfries & Galloway is given by finding the percentage of people in 
that category out of the total of the subset of the population and then using that 
percentage to find the expected number from the input.

For example if a service provider in D&G saw 400 people and 23,101 out of 
149,341 people had no access to a car or van, the service provider could 
expect(23101/149341)*400=62 people who have no access to a car or van (data from 2011 census for D&G area).

The range with confidence interval 95% can be calculated, using a formula that's available on
the method page. 

Features
----------
Description of the features and known bugs can be found in the ReleaseNotes

Technologies Used
-----------------
Spring-boot with thymeleaf
H2 Database Engine
Bootstrap/Jquery

Requirements?
-------------
JDK 1.8+
Maven 3.0+

Install?
----------
Before downloading the project, make sure you have the required version of JDK and Maven installed. 
You might be ask to set your JAVA Home as part of the installation of those. 
After successfully installing those, download the project, go to the folder that contains the pom file 
and run using:mvn spring-boot:run

You don't need to worry about installing Tomcat or Thymelead,Spring-boot takes care of that. 


Running it on the lab machines
-------------------------------
1.ssh to hoved
2.use the mvn3 spring-boot:run command in order to run it, since this is the required version of maven
3.the port has been specified in the application properties and it's 8090, so when open your browser,
open hoved.dcs.gla.ac.uk:8090